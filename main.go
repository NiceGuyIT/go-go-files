package main

import (
	"encoding/json"
	"fmt"
	"github.com/TylerBrock/colorjson"
	"github.com/rs/zerolog/log"
	"os"
)

// files manages files on a gohttpserver server.
// Commands
//   - upload: Upload a file to the server
//       upload file -path=/public/myfiles/ -name=newname
//   - download: Download a file from the server
//       download file -path=/public/ -save=/some/path/filename
//   - install: Download and install a file, restarting a service if necessary
//       install file /path/to/install/file -restartService=myService
//   - search: Search for files on the server
//       search file --format=text
func main() {

	f := newFiles()
	err := f.doCmd()
	if err != nil {
		log.Error().
			Err(err).
			Msg("Error running command")
	}

	/*
		isFile, _ := isFile(file)
		if !isFile {
			log.Error().Str("File does not exist:", file)
			os.Exit(1)
		}
	*/
}

// isFile returns true if path is a file.
func isFile(path string) bool {
	fileInfo, err := os.Stat(path)
	if err != nil {
		return false
	}
	mode := fileInfo.Mode()
	return mode.IsRegular()
}

// isDir returns true if path is a directory.
func isDir(path string) bool {
	fileInfo, err := os.Stat(path)
	if err != nil {
		return false
	}
	mode := fileInfo.Mode()
	return mode.IsDir()
}

// formatJSON converts a byte array to formatted string.
func formatJSON(j []byte) (string, error) {
	var err error
	var obj map[string]interface{}
	var s []byte

	err = json.Unmarshal(j, &obj)
	if err != nil {
		log.Error().
			Err(err).
			Msg("Error unmarshaling the JSON response")
		return "", err
	}
	//f := colorjson.NewFormatter()
	//f.Indent = 4
	//f.DisabledColor = false
	//s, err = f.Marshal(obj)

	s, err = colorjson.Marshal(obj)
	if err != nil {
		log.Error().
			Err(err).
			Msg("Error marshaling the JSON object")
		return "", err
	}
	return fmt.Sprintln(string(s)), nil
}
