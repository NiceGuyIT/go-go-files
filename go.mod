module files

go 1.17

require (
	github.com/TylerBrock/colorjson v0.0.0-20200706003622-8a50f05110d2
	github.com/alecthomas/kong v0.4.1
	github.com/rs/zerolog v1.26.1
)

require (
	github.com/fatih/color v1.13.0 // indirect
	github.com/h2non/filetype v1.1.3 // indirect
	github.com/hokaccha/go-prettyjson v0.0.0-20211117102719-0474bc63780f // indirect
	github.com/mattn/go-colorable v0.1.12 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/sys v0.0.0-20220209214540-3681064d5158 // indirect
)
