# Go Go Gadget Files

Files is a [gohttpserver](https://github.com/codeskyblue/gohttpserver) client to download, upload and search files.
It's meant as a general purpose automation tool to manage files on a file server.

## Building

You can compile in the credentials to the binary, so they don't have to be provided on the command line. Be aware the
credentials can be extracted using `strings`. Since it's common for AV to upload files to their sandbox for testing,
it's likely they will also have your credentials. **Caveat emptor**!

```bash
FILES_USERNAME=go-go-gadget-files
FILES_PASSWORD=go-go-gadget-password
FILES_DOMAIN=download.example.com
FILES_URL_PATH=public

go build -ldflags "\
  -X 'main.domain=$FILES_DOMAIN' \
  -X 'main.urlPath=$FILES_URL_PATH' \
  -X 'main.auth=basic' \
  -X 'main.username=$FILES_USERNAME' \
  -X 'main.password=$FILES_PASSWORD'"
```

## gohttpserver Configuration

Currently, only BASIC auth is supported. Configure [gohttpserver](https://github.com/codeskyblue/gohttpserver) as
follows.

```yml
# Enable uploads
upload: true
auth:
    # BASIC HTTP auth
    type: "http"
    # username:password
    # No hashing
    http: "go-go-gadget-files:go-go-gadget-password"
```

## Usage

Full usage using environmental variables, not compiled in.

> Note: `FILES_AUTH` supports only "basic".

```bash
FILES_DOMAIN=download.example.com
FILES_USERNAME=go-go-gadget-files
FILES_PASSWORD=go-go-gadget-password
FILES_AUTH=basic
files download myfile -urlPath=myfiles/ -save /opt/myfiles/
```

Upload new file.

```bash
files upload newfile -urlPath=public/
```

Search for files.

```bash
files search findme -urlPath=public/
```

## Improvements

There's definite room for improvement. Some thoughts.

- Perform more checks to make sure it acts as desired.
- Encode or encrypt the credentials in the binary.
- Add metadata like file hash, version, architecture, os, etc.
- Restart a service after updating the binary.
