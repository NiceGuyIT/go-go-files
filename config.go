package main

// domain is the domain hosting the files
var domain = ""

// path is the URL path to the files
var urlPath = ""

// auth is basic or token
var auth = ""

// username for basic HTTP auth
var username = ""

// password for basic HTTP auth
var password = ""

// getDomain gets the domain from the build time variable or config variable
func getDomain(build string, config string) string {
	if len(config) > 0 {
		return config
	}
	return build
}

// getPath gets the path from the build time variable or config variable
func getURLPath(build string, config string) string {
	if len(config) > 0 {
		return config
	}
	return build
}

// getAuth gets the auth from the build time variable or config variable
func getAuth(build string, config string) string {
	if len(config) > 0 {
		return config
	}
	return build
}

// getUsername gets the username from the build time variable or config variable
func getUsername(build string, config string) string {
	if len(config) > 0 {
		return config
	}
	return build
}

// getPassword gets the password from the build time variable or config variable
func getPassword(build string, config string) string {
	if len(config) > 0 {
		return config
	}
	return build
}
