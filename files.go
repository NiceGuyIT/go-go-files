package main

import (
	"bytes"
	"fmt"
	"github.com/h2non/filetype"
	"github.com/rs/zerolog/log"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"strings"
)

type config struct {
	cli         cliConfig
	server      serverConfig
	file        fileConfig
	cmd         string
	contentType string `default:"application/json"`
}

type serverConfig struct {
	url *url.URL
}

type fileConfig struct {
	name     string
	file     os.File
	path     string
	filetype string `default:"application/json"`
}

func newFiles() *config {
	cfg := &config{}
	ctx := parseCLI(&cfg.cli)

	switch ctx.Command() {
	case "download <file>":
		log.Debug().
			Msg("Processing download command")
		var err error
		cfg.cmd = "download"
		cfg.server.url = &url.URL{
			Scheme: "https",
			Host:   strings.TrimRight(cfg.cli.Domain, "/"),
			Path:   strings.Trim(getURLPath(urlPath, cfg.cli.Download.URLPath), "/"),
		}
		if err != nil {
			log.Error().
				Err(err).
				Msg("Could not parse URL")
			return nil
		}
		log.Debug().
			Str("url", cfg.server.url.String()).
			Msg("Build the URL")

	case "upload <file>":
		log.Debug().
			Msg("Processing upload command")
		var err error
		cfg.cmd = "upload"
		cfg.server.url = &url.URL{
			Scheme: "https",
			Host:   strings.TrimRight(cfg.cli.Domain, "/"),
			Path:   strings.Trim(getURLPath(urlPath, cfg.cli.Upload.URLPath), "/"),
		}
		if err != nil {
			log.Error().
				Err(err).
				Msg("Could not parse URL")
			return nil
		}
		log.Debug().
			Str("url", cfg.server.url.String()).
			Msg("Build the URL")

	case "search <files>":
		log.Debug().
			Msg("Processing search command")
		var err error
		cfg.cmd = "search"
		cfg.server.url = &url.URL{
			Scheme: "https",
			Host:   strings.TrimRight(cfg.cli.Domain, "/"),
			Path:   strings.Trim(getURLPath(urlPath, cfg.cli.Search.URLPath), "/"),
		}
		if err != nil {
			log.Error().
				Err(err).
				Msg("Could not parse URL")
			return nil
		}
		log.Debug().
			Str("url", cfg.server.url.String()).
			Msg("Build the URL")

	default:
		log.Error().
			Str("command", ctx.Command()).
			Msg("Invalid command line parameters")
		err := ctx.PrintUsage(false)
		if err != nil {
			log.Error().
				Err(err).
				Msg("Invalid command line parameters")
			return nil
		}
		os.Exit(1)
	}

	// TODO: Figure out default values for structs
	cfg.file.filetype = "application/json"
	cfg.contentType = "application/json"

	return cfg
}

func (cfg config) openFile() *os.File {
	file, err := os.Open(cfg.cli.Download.File)
	if err != nil {
		log.Fatal().
			Err(err).
			Str("file", cfg.cli.Download.File).
			Msg("Error opening file")
	}
	defer func(file *os.File) {
		err := file.Close()
		if err != nil {
			log.Error().
				Err(err).
				Str("file", cfg.cli.Download.File).
				Msg("Error closing file")
		}
	}(file)
	return file
}

func (cfg config) openServer(body *bytes.Buffer, writer multipart.Writer) *http.Request {
	// See this post about redirects
	// https://stackoverflow.com/a/31309385
	request, err := http.NewRequest("POST", cfg.server.url.String(), body)
	if err != nil {
		log.Fatal().
			Err(err).
			Msg("Error creating the HTTP client")
	}

	if cfg.cli.Auth.AuthType == "basic" {
		request.SetBasicAuth(cfg.cli.Auth.Username, cfg.cli.Auth.Password)
	}
	request.Header.Add("Content-Type", writer.FormDataContentType())
	return request
}

func (cfg config) cmdDownload() []byte {

	log.Debug().
		Msg("Executing download command")

	var save = ""
	if len(cfg.cli.Download.Save) > 0 {
		if !isDir(cfg.cli.Download.Save) {
			log.Error().
				Str("dir", cfg.cli.Download.Save).
				Msg("Directory does not exist")
			return nil
		}

		save = path.Join(cfg.cli.Download.Save, cfg.cli.Download.File)
	} else {
		save = path.Join(".", cfg.cli.Download.File)
	}

	if isFile(save) {
		log.Error().
			Str("file", save).
			Msg("Not overwriting existing file")
		return nil
	}

	file, err := os.Create(save)
	if err != nil {
		log.Error().
			Err(err).
			Str("file", save).
			Msg("Error creating file")
		return nil
	}
	defer func(file *os.File) {
		err := file.Close()
		if err != nil {
			log.Error().
				Err(err).
				Str("file", save).
				Msg("Error closing downloaded file")
		}
	}(file)

	cfg.server.url.Path = strings.TrimRight(cfg.server.url.Path, "/") + "/" +
		strings.Trim(cfg.cli.Download.File, "/")
	log.Debug().
		Str("url", cfg.server.url.String()).
		Str("urlPath", cfg.server.url.Path).
		Msg("Downloading file from server")

	// See this post about redirects
	// https://stackoverflow.com/a/31309385
	request, err := http.NewRequest("GET", cfg.server.url.String(), nil)
	if err != nil {
		log.Error().
			Err(err).
			Msg("Error creating the HTTP client")
		return nil
	}

	if cfg.cli.Auth.AuthType == "basic" {
		request.SetBasicAuth(cfg.cli.Auth.Username, cfg.cli.Auth.Password)
	}
	client := &http.Client{}

	response, err := client.Do(request)
	if err != nil {
		log.Error().
			Err(err).
			Msg("Error creating the HTTP client")
		return nil
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			log.Error().
				Err(err).
				Msg("Error closing the connection")
		}
	}(response.Body)

	b, err := io.Copy(file, response.Body)
	if err != nil {
		log.Error().
			Err(err).
			Msg("Error copying the file")
		return nil
	}
	log.Info().
		Int64("bytes", b).
		Msg("Downloaded file")

	rsp := fmt.Sprintf(`{
		"success": true,
		"bytes": %d,
		"name": "%s"
	}`, b, save)

	buf, _ := ioutil.ReadFile(save)

	kind, _ := filetype.Match(buf)
	if kind.MIME.Value == "application/x-executable" {
		log.Info().
			Str("mime_value", kind.MIME.Value).
			Msg("File detected as executable. Setting execute bit")
		err = os.Chmod(save, 0755)
		if err != nil {
			log.Info().
				Err(err).
				Str("file", save).
				Msg("Failed to set execute bit on file")
			return nil
		}
	}

	return []byte(rsp)
}

func (cfg config) cmdUpload() []byte {

	log.Debug().
		Str("file", cfg.cli.Upload.File).
		Str("filetype", cfg.file.filetype).
		Msg("Executing upload command")

	file, err := os.Open(cfg.cli.Upload.File)
	if err != nil {
		log.Error().
			Err(err).
			Str("file", cfg.cli.Upload.File).
			Msg("Error opening file")
		return nil
	}
	defer func(file *os.File) {
		err := file.Close()
		if err != nil {
			log.Error().
				Err(err).
				Str("file", cfg.cli.Upload.File).
				Msg("Error closing file")
		}
	}(file)

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile("file", filepath.Base(file.Name()))
	if err != nil {
		log.Error().
			Err(err).
			Msg("Failed to create form")
		return nil
	}

	b, err := io.Copy(part, file)
	if err != nil {
		log.Error().
			Err(err).
			Msg("Error copying the file")
		return nil
	}
	log.Info().
		Int64("bytes", b).
		Msg("Read file for upload")
	err = writer.Close()
	if err != nil {
		log.Error().
			Err(err).
			Msg("Error closing the writer")
		return nil
	}

	log.Debug().
		Str("url", cfg.server.url.String()).
		Msg("Upload URL")

	// See this post about redirects
	// https://stackoverflow.com/a/31309385
	request, err := http.NewRequest("POST", cfg.server.url.String(), body)
	if err != nil {
		log.Fatal().
			Err(err).
			Msg("Error creating the HTTP client")
	}

	if cfg.cli.Auth.AuthType == "basic" {
		log.Debug().
			Msg("Setting BASIC auth")
		request.SetBasicAuth(cfg.cli.Auth.Username, cfg.cli.Auth.Password)
	}
	request.Header.Add("Content-Type", writer.FormDataContentType())
	client := &http.Client{}

	response, err := client.Do(request)
	if err != nil {
		log.Fatal().
			Err(err).
			Msg("Error creating the HTTP client")
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			log.Error().
				Err(err).
				Msg("Error closing the connection")
		}
	}(response.Body)

	content, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Fatal().
			Err(err).
			Bytes("content", content).
			Msg("Error reading the response")
	}

	log.Info().
		Str("content", string(content)).
		Msg("Upload successful")
	return content
}

func (cfg config) cmdSearch() []byte {

	log.Debug().
		Msg("Executing search command")

	q := cfg.server.url.Query()
	q.Set("json", "true")
	q.Set("search", strings.Join(cfg.cli.Search.Files, "%20"))
	cfg.server.url.RawQuery = q.Encode()

	log.Debug().
		Str("url", cfg.server.url.String()).
		Msg("Querying URL")

	// See this post about redirects
	// https://stackoverflow.com/a/31309385
	request, err := http.NewRequest("GET", cfg.server.url.String(), nil)
	if err != nil {
		log.Fatal().
			Err(err).
			Msg("Error creating the HTTP request")
	}

	if cfg.cli.Auth.AuthType == "basic" {
		request.SetBasicAuth(cfg.cli.Auth.Username, cfg.cli.Auth.Password)
	}
	request.Header.Add("Content-Type", "application/json")

	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		log.Error().
			Err(err).
			Msg("Error creating the HTTP client")
		return nil
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			log.Error().
				Err(err).
				Msg("Error closing the connection")
		}
	}(response.Body)

	content, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Error().
			Err(err).
			Bytes("content", content).
			Msg("Error reading the response")
		return nil
	}

	return content
}

func (cfg config) doCmd() error {

	var rsp []byte
	switch cfg.cmd {
	case "upload":
		rsp = cfg.cmdUpload()
	case "download":
		rsp = cfg.cmdDownload()
	case "search":
		rsp = cfg.cmdSearch()
	default:
		log.Error().
			Msg("Invalid command")
		os.Exit(1)
	}

	if rsp == nil {
		log.Info().
			Msg("The command did not produce any output")
		return nil
	}
	j, err := formatJSON(rsp)
	if err != nil {
		log.Error().
			Err(err).
			Str("json", j).
			Msg("Error converting the response to JSON")
		return err
	}
	fmt.Println(j)

	return nil
}
