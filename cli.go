package main

import (
	"github.com/alecthomas/kong"
	"github.com/rs/zerolog/log"
)

type cliConfig struct {
	Download struct {
		File    string `arg:"" name:"file" help:"File to download."`
		URLPath string `name:"urlpath" optional:"" default:"public" help:"URL path to use on the server."`
		Save    string `name:"save" default:"" help:"File to save the downloaded file."`
	} `cmd:"" help:"Download a file from the server."`

	Upload struct {
		File    string `arg:"" name:"file" help:"File to upload."`
		URLPath string `name:"urlpath" optional:"" default:"public" help:"URL path to use on the server."`
	} `cmd:"" help:"Upload a file to the server."`

	Search struct {
		Files   []string `arg:"" name:"files" help:"File(s) to search for."`
		URLPath string   `name:"urlpath" optional:"" default:"public" help:"URL path to use on the server."`
	} `cmd:"" help:"Search for files on the server."`

	Logging struct {
		// https://github.com/rs/zerolog#leveled-logging
		Level string `enum:"debug,info,warn,error" default:"info"`
		Type  string `enum:"json,console" default:"console"`
	} `embed:"" prefix:"logging."`

	Auth struct {
		AuthType string `name:"auth" envprefix:"FILES_" env:"AUTH" default:"basic"`
		Username string `name:"username" envprefix:"FILES_" env:"USERNAME"`
		Password string `name:"password" envprefix:"FILES_" env:"PASSWORD"`
	} `embed:"" prefix:"auth."`

	Domain string `name:"domain" envprefix:"FILES_" env:"DOMAIN" help:"Domain hosting the files."`
}

func parseCLI(cli *cliConfig) *kong.Context {
	ctx := kong.Parse(cli,
		kong.UsageOnError(),
		kong.ConfigureHelp(kong.HelpOptions{
			Summary: false,
		}))

	setLogging(cli)

	// Check for build-time variables
	cli.Domain = getDomain(domain, cli.Domain)
	cli.Auth.AuthType = getAuth(auth, cli.Auth.AuthType)
	cli.Auth.Username = getUsername(username, cli.Auth.Username)
	cli.Auth.Password = getPassword(password, cli.Auth.Password)

	log.Debug().
		Str("domain", cli.Domain).
		Str("auth", cli.Auth.AuthType).
		Str("username", cli.Auth.Username).
		Str("password", "REDACTED").
		Msg("Build time variables")

	return ctx
}
